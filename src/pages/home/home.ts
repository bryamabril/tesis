import { Component } from '@angular/core';
import { NavController, ToastController,IonicPage, NavParams } from 'ionic-angular';

import { ContactProvider,ContactList } from '../../providers/contact/contact';
import {VideoPage} from '../../pages/video/video'

import {AngularFireAuth} from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  contacts: ContactList[];
  
  
  constructor(public navCtrl: NavController, private contactProvider: ContactProvider, private toast: ToastController,private autentica:AngularFireAuth, public navParams: NavParams) {
     
  }

  ionViewDidLoad() {
    this.autentica.authState.subscribe(data => {
    	this.toast.create({
    		message: 'Bienvenido',
    		duration: 4000
    	}).present();
    });
  }


  ionViewDidEnter() {
    this.contactProvider.getAll()
      .then((result) => {
        this.contacts = result;
      });
   
  }
  
  addContact() {
    this.navCtrl.push('EditContactPage');
  }

  editContact(item: ContactList) {
    this.navCtrl.push('EditContactPage', { key: item.key, contact: item.contact });
  }

  removeContact(item: ContactList) {
    this.contactProvider.remove(item.key)
      .then(() => {
        // Remueve el array
        var index = this.contacts.indexOf(item);
        this.contacts.splice(index, 1);
        this.toast.create({ message: 'Camara Eliminada', duration: 3000, position: 'botton' }).present();
      })
  }
  irpagina():void{
    this.navCtrl.push(VideoPage);
  }
  
  
}