import { Component } from '@angular/core';
import { IonicPage, NavController,ToastController} from 'ionic-angular';
import { ContactProvider,ContactList } from '../../providers/contact/contact';
import {SmsPage} from '../sms/sms'
import { SMS } from '@ionic-native/sms';


@IonicPage()
@Component({
  selector: 'page-video',
  templateUrl: 'video.html',
})
export class VideoPage {
  contacts: ContactList[];
    
  
  constructor(public navCtrl: NavController, private contactProvider: ContactProvider, public toastCtrl: ToastController,private smsVar: SMS) {
   
    this.contactProvider.getAll()
    .then((result) => {
      this.contacts = result;
      
      function replacer(key, value) {
        if (key === 'key') {
         return undefined;
       }
       return value;
      }
      var userStr = JSON.stringify(result, replacer);
      //document.write(userStr); 
      //var nueva=JSON.parse(userStr)
      const ApiKey = userStr.substr(22,30);
      const GroupKey=userStr.substr(63,10);
      const MonitorID=userStr.substr(84,10);
      console.log(ApiKey);
      console.log(GroupKey);
      console.log(MonitorID); 
      
    });
    
    
  }

  sendSMS(){
    var options={
          replaceLineBreaks: false, // true to replace \n by a new line, false by default
          android: {
              // intent: 'INTENT'  // Opens Default sms app
              intent: '' // Sends sms without opening default sms app
            }
    }
    this.smsVar.send('+593995049854', 'Activar',options)
      .then(()=>{
        alert("Activado");
      },()=>{
      alert("Error");
      });
  }
  irpagina():void{
    this.navCtrl.push(SmsPage);
  }
  
}
 
