import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
//import { HomePage } from '../pages/home/home';

import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { ContactProvider } from '../providers/contact/contact';
import {VideoPageModule} from '../pages/video/video.module'
import { StreamingMedia } from '@ionic-native/streaming-media';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { AngularFireModule } from "angularfire2";
import { FIREBASE_CONFIG } from "./app.firebase.config";
import {AngularFireAuthModule} from "angularfire2/auth";

//mapas
import { Geolocation } from '@ionic-native/geolocation';
import { GoogleMaps } from '@ionic-native/google-maps';

import { MapasPage } from '../pages/mapas/mapas';
//sms mostrar
import {FirstCapsPipe} from '../pipes/first-caps/first-caps';
import {AndroidPermissions} from '@ionic-native/android-permissions'
import { SmsPage } from '../pages/sms/sms';
//sms enviar
import {SMS} from '@ionic-native/sms';

@NgModule({
  declarations: [
    MyApp,
    SmsPage,
    MapasPage,
    FirstCapsPipe
   // HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    VideoPageModule,
    BrowserModule,
    AngularFireModule.initializeApp(FIREBASE_CONFIG),
    AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SmsPage,
    MapasPage
  //  HomePage
  ],
  providers: [
    AndroidPermissions,
    StatusBar,
    InAppBrowser,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DatePipe,
    ContactProvider,
    Geolocation,
    GoogleMaps,
    SMS,
    StreamingMedia
  ]
})
export class AppModule {}