import { NgModule } from '@angular/core';
import { EnviromentDirective } from './enviroment/enviroment';
@NgModule({
	declarations: [EnviromentDirective],
	imports: [],
	exports: [EnviromentDirective]
})
export class DirectivesModule {}
