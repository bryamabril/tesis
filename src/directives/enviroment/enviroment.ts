import { Directive } from '@angular/core';

/**
 * Generated class for the EnviromentDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[enviroment]' // Attribute selector
})
export class EnviromentDirective {

  constructor() {
    console.log('Hello EnviromentDirective Directive');
  }

}
